package model;

public class Etude extends Model {
    
    int id;
    String niveau;

    

    public Etude() {
        setFieldNumber(2);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNiveau() {
        return niveau;
    }
    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
}
