package model;

import java.sql.Connection;
import java.sql.PreparedStatement;

import data.Connect;

public class EmployeSpecialite extends Model {
    int idEmploye;
    int idSpecialite;

    public EmployeSpecialite(int idEmploye, int idSpecialite) {
        setIdEmploye(idEmploye);
        setIdSpecialite(idSpecialite);
    }
    public EmployeSpecialite() {
    }

    public void insert(Connection conn) throws Exception {
        boolean indepConn = false;
        if(conn == null) {
            conn = new Connect().setConnection();
            indepConn = true;
        }

        String query = "insert into employespecialite values (?, ?)";

        try {
            PreparedStatement stat = conn.prepareStatement(query);
            stat.setInt(1, this.getIdEmploye());
            stat.setInt(2, this.getIdSpecialite());

            stat.executeUpdate();
        } catch (Exception e) {
            throw new Exception("Cannot insert on employespecialite : "+ e.getMessage());
        } finally {
            if(indepConn) {
                conn.close();
            }
        }
    }

    public int getIdEmploye() {
        return idEmploye;
    }
    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }
    public int getIdSpecialite() {
        return idSpecialite;
    }
    public void setIdSpecialite(int idSpecialite) {
        this.idSpecialite = idSpecialite;
    }
}
