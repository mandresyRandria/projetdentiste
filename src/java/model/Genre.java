package model;

public class Genre extends Model {
    
    int id;
    String label;

    

    public Genre() {
        setFieldNumber(2);
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
}
