package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;

import data.Connect;

public class Employe extends Model {
    
    int id;
    String nom;
    String prenom;
    Date dateNaissance;
    int idEtude;
    int idGenre;

    public Employe(String nom, String prenom, Date dateNaissance, int idEtude, int idGenre) throws Exception {
        setNom(nom);
        setPrenom(prenom);
        setDateNaissance(dateNaissance);
        setIdEtude(idEtude);
        setIdGenre(idGenre);
        setFieldNumber(6);
    }

    public Employe() {
        setFieldNumber(6);
    }

    public Employe findByAllProps(Connection conn) throws Exception {
        boolean indepConn = false;
        if(conn == null) {
            conn = new Connect().setConnection();
            indepConn = true;
        }
        String query = "select * from employe where nom=? and prenom=? and datenaissance=? and idetude=? and idgenre=?";

        // String query = "select * from employe where nom="+nom+", prenom="+prenom+", datenaissance="+dateNaissance+", idetude="+idEtude+", idgenre="+idGenre+"";

        try {
            // Statement stat = conn.createStatement();
            PreparedStatement stat = conn.prepareStatement(query);
            stat.setString(1, this.nom);
            stat.setString(2, this.prenom);
            stat.setDate(3, this.dateNaissance);
            stat.setInt(4, this.idEtude);
            stat.setInt(5, this.idGenre);

            ResultSet res = stat.executeQuery();
            Employe employe = null;
            while(res.next()) {
                employe = new Employe(res.getString(2), res.getString(3), res.getDate(4), res.getInt(5), res.getInt(6));
                employe.setId(res.getInt(1));
            }

            return employe;

        } catch (Exception e) {
            throw new Exception("Can't find employe with all props "+ e.getMessage());
        } finally {
            if(indepConn) {
                conn.close();
            }
        }
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public Date getDateNaissance() {
        return dateNaissance;
    }
    public void setDateNaissance(Date dateNaissance) throws Exception {
        int currentYear = LocalDate.now().getYear();
        String date = dateNaissance.toString();
        String[] splitedDate = date.split("-");
        int annee = Integer.parseInt(splitedDate[0]);
        System.out.println(currentYear);
        if((currentYear - annee) < 18) {
            throw new Exception("Age invalide");
        }
        this.dateNaissance = dateNaissance;
    }

    // public void insert(Connection conn) throws Exception {
    //     String query = "insert into employe values (default, '"+this.nom+"', '"+this.prenom+"', '"+this.dateNaissance+"', "+this.idEtude+", "+this.idSpecialite+", "+this.idGenre+")";

    //     System.out.println(query);

    //     try {
    //         Statement stat = conn.createStatement();
    //         stat.executeQuery(query);
    //     } catch (Exception e) {
    //         throw new Exception("Unable to insert emp");
    //     }
    //     finally {
    //         conn.close();
    //     }
    // }

    public int getIdEtude() {
        return idEtude;
    }
    public void setIdEtude(int idEtude) {
        this.idEtude = idEtude;
    }
    public int getIdGenre() {
        return idGenre;
    }
    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }

    
}
