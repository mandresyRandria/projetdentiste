package model;

public class Specialite extends Model {
    
    int id;
    String label;
    
    public Specialite() {
        setFieldNumber(2);
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    
}
