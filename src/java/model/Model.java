package model;

import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import data.Connect;


public class Model {

    int zeronumber;
    int fieldNumber;

    public Vector findAll(Connection conn, boolean filter) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            independentConn = true;
        }
        Vector data = new Vector<>();
        String req = "select * from " + this.getClass().getSimpleName().toLowerCase();
        if(filter == true) {
            Field[] fileds = this.getClass().getDeclaredFields();
            int fieldCounter = 0;
            for (Field field : fileds) {
                if (field.getType() == String.class && field.get(this) != null) {
                    fieldCounter++;
                    if (fieldCounter == 1) {
                        req += " where";
                    } else {
                        req += " and";
                    }
                    req += " " + field.getName().toLowerCase() + " = '" + field.get(this) + "'";
                }
            }
        }
        System.out.println(req);
        try {
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(req);
            while (res.next()) {
                Object temp = this.getClass().getConstructor().newInstance();
                Field[] tempFields = temp.getClass().getDeclaredFields();
                for (int i = 0; i < fieldNumber; i++) {
                    Method geter = ResultSet.class.getDeclaredMethod(
                            "get" + tempFields[i].getType().getSimpleName().replace(
                                    tempFields[i].getType().getSimpleName().charAt(0),
                                    tempFields[i].getType().getSimpleName().toUpperCase().charAt(0)),
                            String.class);
                    tempFields[i].setAccessible(true);

                    String nameFormat = tempFields[i].getName().replaceFirst(
                            String.valueOf(tempFields[i].getName().charAt(0)),
                            String.valueOf(tempFields[i].getName().toUpperCase().charAt(0)));
                    Method setter = this.getClass().getDeclaredMethod("set" + nameFormat, tempFields[i].getType());
                    setter.invoke(temp, geter.invoke(res, tempFields[i].getName().toLowerCase()));
                    // tempFields[i].set(temp, geter.invoke(res,
                    // tempFields[i].getName().toLowerCase()));
                }
                data.add(temp);
            }

        } catch (SQLException e) {
            throw new Exception("UNABLE TO GET DATA : " + e.getLocalizedMessage() + " => " + e.getCause());
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        } finally {
            if (independentConn == true) {
                conn.close();
            }
        }
        return data;
    }

    public Object find(Connection conn) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            independentConn = true;
        }

        Object valiny = this.getClass().getConstructor().newInstance();
        Field[] fields = this.getClass().getDeclaredFields();

        String req = "select * from "+this.getClass().getSimpleName().toLowerCase()+" where "+fields[0].getName().toLowerCase()+"='"+fields[0].get(this)+"'";
        System.out.println(req);

        try {
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(req);

            int iField = 0;
            while(res.next()) {
                for(int i = 0; i<fieldNumber; i++) {
                    fields[i].setAccessible(true);
                    Method geter = ResultSet.class.getDeclaredMethod(
                                "get" + fields[i].getType().getSimpleName().replace(
                                        fields[i].getType().getSimpleName().charAt(0),
                                        fields[i].getType().getSimpleName().toUpperCase().charAt(0)),
                                String.class);
                    String nameFormat = fields[i].getName().replaceFirst(String.valueOf(fields[i].getName().charAt(0)),String.valueOf(fields[i].getName().toUpperCase().charAt(0)));
                    Method setter = this.getClass().getDeclaredMethod("set" + nameFormat, fields[i].getType());
                    // System.out.println(setter.getName());
                    setter.invoke(valiny, geter.invoke(res, fields[i].getName().toLowerCase()));
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Unable to find one");
        } finally {
            if(independentConn) {
                conn.close();
            }
        }

        return valiny;
    }

    public Vector findBy(Connection conn, String fieldName, String value) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            independentConn = true;
        }
        Vector data = new Vector<>();
        String req = "select * from " + this.getClass().getSimpleName().toLowerCase()+" where "+fieldName+"="+value;
        System.out.println(req);
        try {
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(req);
            while (res.next()) {
                Object temp = this.getClass().getConstructor().newInstance();
                Field[] tempFields = temp.getClass().getDeclaredFields();
                for (int i = 0; i < fieldNumber; i++) {
                    Method geter = ResultSet.class.getDeclaredMethod(
                            "get" + tempFields[i].getType().getSimpleName().replace(
                                    tempFields[i].getType().getSimpleName().charAt(0),
                                    tempFields[i].getType().getSimpleName().toUpperCase().charAt(0)),
                            String.class);
                    tempFields[i].setAccessible(true);

                    String nameFormat = tempFields[i].getName().replaceFirst(
                            String.valueOf(tempFields[i].getName().charAt(0)),
                            String.valueOf(tempFields[i].getName().toUpperCase().charAt(0)));
                    Method setter = this.getClass().getDeclaredMethod("set" + nameFormat, tempFields[i].getType());
                    setter.invoke(temp, geter.invoke(res, tempFields[i].getName().toLowerCase()));
                    // tempFields[i].set(temp, geter.invoke(res,
                    // tempFields[i].getName().toLowerCase()));
                }
                data.add(temp);
            }

        } catch (SQLException e) {
            throw new Exception("UNABLE TO GET DATA : " + e.getLocalizedMessage() + " => " + e.getCause());
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        } finally {
            if (independentConn == true) {
                conn.close();
            }
        }
        return data;
    }

    // public Object findBy(String fieldname, String fieldvalue, Connection conn) throws Exception {
    //     boolean independentConn = false;
    //     if (conn == null) {
    //         conn = new Connect().setConnection();
    //         independentConn = true;
    //     }

    //     Object valiny = this.getClass().getConstructor().newInstance();
    //     Field[] fields = this.getClass().getDeclaredFields();
    //     Field predicat = null;
    //     for (Field field : fields) {
    //         if(field.getName().toLowerCase().equals(fieldname.toLowerCase())) {
    //             predicat = field;
    //         }
    //     }

    //     String req = "select * from "+this.getClass().getSimpleName().toLowerCase()+" where "+predicat.getName().toLowerCase()+"='"+fieldvalue+"'";
    //     System.out.println(req);

    //     try {
    //         Statement stat = conn.createStatement();
    //         ResultSet res = stat.executeQuery(req);

    //         int iField = 0;
    //         while(res.next()) {
    //             for(Field inuse : fields) {
    //                 inuse.setAccessible(true);
    //                 Method geter = ResultSet.class.getDeclaredMethod(
    //                             "get" + inuse.getType().getSimpleName().replace(
    //                                     inuse.getType().getSimpleName().charAt(0),
    //                                     inuse.getType().getSimpleName().toUpperCase().charAt(0)),
    //                             String.class);
    //                 String nameFormat = inuse.getName().replaceFirst(String.valueOf(inuse.getName().charAt(0)),String.valueOf(inuse.getName().toUpperCase().charAt(0)));
    //                 Method setter = this.getClass().getDeclaredMethod("set" + nameFormat, inuse.getType());
    //                 // System.out.println(setter.getName());
    //                 setter.invoke(valiny, geter.invoke(res, inuse.getName().toLowerCase()));
    //             }
                
    //         }
    //     } catch (Exception e) {
    //         throw new Exception("Unable to find one");
    //     } finally {
    //         if(independentConn) {
    //             conn.close();
    //         }
    //     }

    //     return valiny;
    // }

    public void insert(Connection conn) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            conn.setAutoCommit(false);
            independentConn = true;
        }

        try {
            // String id = this.getSequenceValue(conn);
            // Field idField = this.getClass().getDeclaredField("id");
            // idField.setAccessible(true);
            // idField.set(this, id);

            // this.insertHist("insert", conn);
            /* -------------- SET ID ------------- */
            // int zeronumber = 6;
            /* ------------------------------------- */

            Statement stat = conn.createStatement();
            String query = "insert into " + this.getClass().getSimpleName().toLowerCase() + " values (default,";
            Field[] fields = this.getClass().getDeclaredFields();
            fields[0].setAccessible(true);
            // fields[0].setInt(this, Integer.parseInt(id));
            int commaCount = 2;
            for (int i = 1; i < fields.length; i++) {
                fields[i].setAccessible(true);
                System.out.println(fields[i].get(this).toString());
                if (fields[i].getType() == Date.class) {
                    if (fields[i].get(this) == null) {
                        query += "null";
                    } else {
                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String formatedDate = df.format(fields[i].get(this));
                        query += "'" + formatedDate + "'";
                    }
                } else if (fields[i].getType() != String.class) {
                    System.out.println(fields[i].get(this).toString());
                    query += fields[i].get(this).toString();
                } else {
                    query += "'" + fields[i].get(this) + "'";
                }

                if (commaCount < fields.length) {
                    query += ",";
                }
                commaCount++;
            }
            query += ")";

            System.out.println(query);
            stat.executeUpdate(query);

            
            if(independentConn) {
                conn.commit();
            }
            
            // return res;
        } catch (Exception e) {
            throw new Exception("UNABLE TO INSERT : " + e.getMessage());
            // e.printStackTrace();
        } finally {
            if (independentConn == true) {
                conn.close();
            }
        }
    }

    public void update(Connection conn) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            conn.setAutoCommit(false);
            independentConn = true;
        }

        String req = "update " + this.getClass().getSimpleName().toLowerCase() + " set ";
        Field[] fields = this.getClass().getDeclaredFields();
        int commaCount = 2;
        for (int i = 1; i < fieldNumber; i++) {
            fields[i].setAccessible(true);
            if (fields[i].getType() == Date.class) {
                if (fields[i].get(this) == null) {
                    req += fields[i].getName() + "=null";
                } else {
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formatedDate = df.format(fields[i].get(this));
                    req += fields[i].getName() + "='" + formatedDate + "'";
                }
            } else if (fields[i].getType() != String.class) {
                req += fields[i].getName() + "=" + fields[i].get(this);
            } else {
                req += fields[i].getName() + "='" + fields[i].get(this) + "'";
            }

            if (commaCount < fields.length) {
                req += ",";
            }

            commaCount++;
        }
        if (fields[0].getType() != String.class) {
            req += " where " + fields[0].getName().toLowerCase() + "=" + fields[0].get(this);
        } else {
            req += " where " + fields[0].getName().toLowerCase() + "='" + fields[0].get(this) + "'";
        }

        System.out.println(req);

        try {
            // insertHist("update", conn);
            Statement stat = conn.createStatement();
            stat.executeUpdate(req);
            conn.commit();
        } catch (Exception e) {
            throw new Exception("UNABLE TO UPDATE : " + e.getMessage());
        } finally {
            if (independentConn == true) {
                conn.rollback();
                conn.close();
            }
        }
    }

    

    public String getSequenceValue(Connection conn) throws Exception {
        boolean independentConn = false;
        if (conn == null) {
            conn = new Connect().setConnection();
            independentConn = true;
        }

        String val = "";
        String tableName = this.getClass().getSimpleName().toLowerCase();

        String req = "select " + tableName + "_seq.nextval from dual";
        System.out.println(req);
        try {
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(req);
            while (res.next()) {
                val = res.getString(1);
            }
        } catch (Exception e) {
            throw new Exception("failed to get sequence value : " + e.getMessage());
        } finally {
            if (independentConn) {
                conn.close();
            }
        }

        return val;
    }

    public String generateId(Connection conn) throws Exception {
        String id = "";
        try {
            String numericId = this.getSequenceValue(conn);
            int numericIdLength = numericId.length();

            id += this.getClass().getSimpleName().toUpperCase();
            for (int i = 0; i < this.zeronumber - numericIdLength; i++) {
                id += "0";
            }
            id += numericId;
        } catch (Exception e) {
            throw new Exception("Cannot get sequence value : " + e.getMessage());
        }

        return id;
    }

    public int getZeronumber() {
        return zeronumber;
    }

    public void setZeronumber(int zeronumber) {
        this.zeronumber = zeronumber;
    }

    public int getFieldNumber() {
        return fieldNumber;
    }

    public void setFieldNumber(int fieldNumber) {
        this.fieldNumber = fieldNumber;
    }

}
