package data;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connect {
    public Connection setConnection() {
        String host = "localhost";
        // String port = "5432";
        String dbname = "dentiste";
        String url = "";
        try {
            Class.forName("org.postgresql.Driver");
            url = "jdbc:postgresql://"+host+"/"+dbname+"";
            Connection c = DriverManager.getConnection(url, "postgres", "postgresndresy");
            return c;
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
