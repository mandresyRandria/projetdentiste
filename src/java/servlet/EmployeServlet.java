package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Vector;

import data.Connect;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Etude;
import model.Genre;
import model.Specialite;

@WebServlet(name="EmployeServlet", urlPatterns = {"/EmployeServlet"})
public class EmployeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Connection conn = new Connect().setConnection();
        PrintWriter out = res.getWriter();

        // var error
        int error = 0;
        try {
            // donnees ho an ny form
            Vector<Etude> etudes = new Etude().findAll(conn, false);
            Vector<Genre> genres = new Genre().findAll(conn, false);
            Vector<Specialite> specialites = new Specialite().findAll(conn, false);

            out.print("spec size => "+specialites.get(0).getLabel());            

            req.setAttribute("genres", genres);
            req.setAttribute("etudes", etudes);
            req.setAttribute("specialites", specialites);
            
        } catch (Exception e) {
            error = 1;
            out.print("error => "+e);
            // res.sendRedirect("index.jsp");
        }
        finally{
            req.setAttribute("error", error);
            req.getRequestDispatcher("employe.jsp").forward(req, res);    
        }
    }
}
