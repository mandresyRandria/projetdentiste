package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Vector;

import data.Connect;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Employe;
import model.EmployeSpecialite;
import model.Etude;
import model.Genre;
import model.Specialite;

@WebServlet(name = "InsertEmployeServlet", urlPatterns = { "/InsertEmployeServlet" })
public class InsertEmployeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // var error
        int error = 0;

        Connection conn = new Connect().setConnection();
        PrintWriter out = res.getWriter();
        try {
            conn.setAutoCommit(false);
            // donnees ho an ny form
            Vector<Etude> etudes = new Etude().findAll(conn, false);
            Vector<Etude> genres = new Genre().findAll(conn, false);
            Vector<Etude> specialites = new Specialite().findAll(conn, false);

            req.setAttribute("genres", genres);
            req.setAttribute("etudes", etudes);
            req.setAttribute("specialites", specialites);

            // Donnees avy any am form
            String nom = req.getParameter("nom");
            String prenom = req.getParameter("prenom");
            Date dateNaissance = Date.valueOf(req.getParameter("datedenaissance"));
            int genre = Integer.parseInt(req.getParameter("genre"));
            int etude = Integer.parseInt(req.getParameter("etude"));
            out.print("etude => " + etude + "\n");
            String[] specialite = req.getParameterValues("specialite");

            Employe emp = new Employe(nom, prenom, dateNaissance, etude, genre);
            emp.insert(conn);

            emp = emp.findByAllProps(conn);

            out.print("emp => " + emp.getId() + "\n");

            for (int i = 0; i < specialite.length; i++) {
                if (!specialite[i].equals("")) {
                    int idSpecialite = Integer.parseInt(specialite[i]);
                    out.print("spec => " + specialite[i] + "\n");
                    EmployeSpecialite empSpec = new EmployeSpecialite(emp.getId(), idSpecialite);
                    empSpec.insert(conn);
                }
            }

            out.print("vita => " + error);
            conn.commit();
        } catch (Exception e) {
            error = 1;
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            out.print("exception => " + e.getMessage());
        } finally {
            req.setAttribute("error", error);  
            try {
                conn.close();
            } catch (SQLException e) {

            }
            req.getRequestDispatcher("employe.jsp").forward(req, res);  
        }
    }
}
