create database dentiste;

\c dentiste;

create table etude (id SERIAL,
                    niveau VARCHAR(100),
                    primary key(id));

insert into etude values(default,'Bacc'),
                        (default,'Licence'),
                        (default,'Master'),
                        (default,'Doctorat'); 

create table specialite (id SERIAL,
                        label VARCHAR(100),
                        salaire double precision,
                        primary key(id));

insert into specialite values(default,'Theoricien',200000),
                        (default,'Secretaire',200000),
                        (default,'Medecin',500000),                    
                        (default,'Assistant',200000);
insert into specialite(label,salaire) values('Endodontiste',300000);
insert into specialite(label,salaire) values('Chirurgiste buccale',800000);
insert into specialite(label,salaire) values('Radiologue buccale et maxillofaciale',700000);
insert into specialite(label,salaire) values('Dentiste pédiatrique',750000);
insert into specialite(label,salaire) values('Parodontologiste',600000);


create table genre (id SERIAL,
                    label VARCHAR(100),
                    primary key(id));

insert into genre values(default,'Homme'),
                        (default,'Femme');

create table employe(id SERIAL primary key,
                    nom VARCHAR(100),
                    prenom VARCHAR(100),
                    datenaissance date not null,
                    idEtude integer,
                    idGenre integer,
                    foreign key (idEtude) references etude(id),
                    foreign key (idGenre) references genre(id));
 
 create table service (
    id SERIAL primary key,
    label VARCHAR(200)
 );
insert into service(label) values('prothèse');
insert into service(label) values('détartrage');
insert into service(label) values('plombage');
insert into service(label) values('couronnes');

create table employespecialite(
   idemploye integer not null references employe(id),
   idSpecialite integer not null references specialite(id)
);