<%@page import="model.Specialite"%>
<%@page import="model.Etude"%>
<%@page import="java.util.Vector"%>
<%@page import="model.Genre"%>
<% 
    Vector<Genre> listegenre = (Vector<Genre>)request.getAttribute("genres");
    Vector<Etude> listeEtude = (Vector<Etude>)request.getAttribute("etudes");
    Vector<Specialite> listespecialite = (Vector<Specialite>)request.getAttribute("specialites");
%>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->
<html>
    <head>
        <title>Insertion employe</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">

    </head>
    <body>
    <div class="formulaire">

        <div class="dentiste">
            <h1>DENTISTE</h1>
            <p><a href="">Rendez-vous</a></p> 
            <p><a href="">Traitement</a></p> 
            <p><a href="">Mouvement</a></p> 
            <p><a href="">Clients</a></p> 
            <p><a href="">Employ�s</a></p> 
            <p><a href="">Chiffres</a> </p>
        </div>
        <div class="form-insertion">
            <form action="InsertEmployeServlet" method="post">
                <div class="identite">
                    <h3>Ins�rer employ�</h3>
                    <p>Nom</p>
                    <p><input type="text" name="nom" id="nom" required></p>
                    <p>Pr�nom</p>
                    <p><input type="text" name="prenom" id="prenom" required></p>
                    <p>Date de Naissance</p>
                    <%if(request.getAttribute("error").toString().equals("1")) {%>
                    <b style="color: red;">Vous �tes mineur(e)</b>
                    <%}%>
                    <p><input type="date" name="datedenaissance" id="datedenaissance"></p>
                    <p>Genre</p>
                    <p>
                        <select name="genre" id="genre" required>
                            <% for(int i=0;i<listegenre.size();i++){    %>
                            <option value="<%= listegenre.get(i).getId() %>"><%= listegenre.get(i).getLabel() %></option>
                            <%     } %>
                        </select>
                    </p>
                    <p>Etude</p>
                    <p>
                        <select name="etude" id="etude" required>
                            <% for(int i=0;i<listeEtude.size();i++){    %>
                            <option value="<%= listeEtude.get(i).getId() %>"><%= listeEtude.get(i).getNiveau() %></option>
                            <%    } %>
                        </select>
                    </p>
                </div>
                <div class="allspecialite">
                    <br><br>
                    <p>Sp�cialit�</p>
                    <div class="specialite">
                       <% for(int i=0;i<listespecialite.size();i++){    %>
                       <p><input type="checkbox" name="specialite" value="<%= listespecialite.get(i).getId() %>" id=""><%= listespecialite.get(i).getLabel() %></p>
                       <%    } %>
                    </div>
                    <div class="insere"><p><input type="submit" value="Ins�rer"></p></div>
                </div>
                </form>
        </div>
    </div>
    </body>
</html>
